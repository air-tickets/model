
# Models

Install godep

```sh
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
```

Install dependencies

```sh
dep ensure
```

Help

```sh
go run model.go --help
```

## Tickets

### Run without params

```sh
go run model.go airtickets
```

### Help

```sh
go run model.go airtickets --help
```

### Run with params to console

```sh
go run model.go airtickets \
    --duration=50 \
    --airports=1500 \
    --airlines=30 \
    --agencies=200 \
    --passengers=2000000 \
    --purchases=2 \
    --lag=60 \
    --duration=50
```

### Run with params to file

```sh
go run model.go airtickets \
    --duration=10 \
    --file=actions \
    --airports=1500 \
    --airlines=30 \
    --agencies=200 \
    --passengers=2000000000 \
    --purchases=2 \
    --lag=60 \
    --duration=10
```

### Run with params to database

Start postgres in docker

```sh
(docker stop models-db || true) && \
(docker rm models-db || true) && \
docker run \
    --name models-db \
    -d \
    -e POSTGRES_PASSWORD=testpass \
    -e POSTGRES_USER=models \
    -p 7432:5432 \
    postgres
```

Run generation

```sh
go run model.go airtickets \
    --duration=10 \
    --database="host=localhost port=7432 user=models dbname=models password=testpass sslmode=disable" \
    --airports=1500 \
    --airlines=30 \
    --agencies=200 \
    --passengers=2000000000 \
    --purchases=2 \
    --lag=60 \
    --duration=10
```

Check tables

```sh
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT * FROM pg_catalog.pg_tables;"
```

Check generations

```sh
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT DISTINCT generation_name, generation_version FROM actions LIMIT 10;"
```

Check actions

```sh
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT * FROM actions LIMIT 10;"
```

Check payloads

```sh
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT * FROM new_ticket_payloads LIMIT 10;"
```

Check generations

```sh
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT * FROM generations LIMIT 10;"
```

### Testing

#### Help

```sh
go run model.go test --help
```

#### Run without params

```sh
go run model.go test
```

#### Run with params

```sh
go run model.go test \
    --loop=false \
    --model=airtickets
```

#### Run in loop

```sh
go run model.go test \
    --loop=true \
    --model=airtickets
```

## Quotes

### Run without params

```sh
go run model.go quotes
```

### Help

```sh
go run model.go quotes --help
```

### Run with params to console

```sh
go run model.go quotes \
    --duration=5 \
    --pairs=10 \
    --frequency=30 \
    --clients=10000 \
    --change-freq=2 \
    --change-percent=1000 \
    --random-count=100 \
    --random-freq=50
```

### Run with params to file

```sh
go run model.go quotes \
    --duration=30 \
    --file=actions \
    --pairs=10 \
    --frequency=30 \
    --clients=10000 \
    --change-freq=2 \
    --change-percent=1000 \
    --random-count=100 \
    --random-freq=50
```

### Run with params to database

Start postgres in docker

```sh
(docker stop models-db || true) && \
(docker rm models-db || true) && \
docker run \
    --name models-db \
    -d \
    -e POSTGRES_PASSWORD=testpass \
    -e POSTGRES_USER=models \
    -p 7432:5432 \
    postgres
```

Run generation

```sh
go run model.go quotes \
    --duration=10 \
    --database="host=localhost port=7432 user=models dbname=models password=testpass sslmode=disable" \
    --pairs=10 \
    --frequency=30 \
    --clients=10000 \
    --change-freq=1 \
    --change-percent=1000 \
    --random-count=100 \
    --random-freq=50
```

Check actions

```sh
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT * FROM actions LIMIT 10;"
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT COUNT(*) FROM actions;"
```

Check payloads

```sh
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT * FROM new_quote_payloads LIMIT 10;"
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT * FROM client_connect_payloads LIMIT 10;"
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT * FROM client_disconnect_payloads LIMIT 10;"
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT * FROM read_random_payloads LIMIT 10;"

PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT COUNT(*) FROM new_quote_payloads;"
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT COUNT(*) FROM client_connect_payloads;"
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT COUNT(*) FROM client_disconnect_payloads;"
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT COUNT(*) FROM read_random_payloads;"
```

Check generations

```sh
PGPASSWORD=testpass psql -h localhost -p 7432 -d models -U models -w -c "SELECT * FROM generations LIMIT 10;"
```

## Contributing

### Preparing

```sh
# Installing golangci-lint
curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(go env GOPATH)/bin latest
```

### Before commit

```sh
cd $GOPATH/src/gitlab.com/air-tickets/model/ && \

# Static checks
go vet $(go list ./... | grep -v /vendor/) && \
# golangci-lint run --fast && \
# golangci-lint run && \
golangci-lint run --enable-all
```