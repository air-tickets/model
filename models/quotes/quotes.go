package quotes

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/air-tickets/model/action"
	"gitlab.com/air-tickets/model/utils"
)

const (
	secondsInDay     = 24 * 3600
	NewQuote         = "NewQuote"
	ClientConnect    = "ClientConnect"
	ClientDisconnect = "ClientDisconnect"
	ReadRandom       = "ReadRandom"
	bufferLen        = 100
)

type ModelQuote struct {
	Client        string
	Duration      int64
	Pairs         int64
	Frequency     int64
	Clients       int64
	ChangeFreq    int64
	ChangePercent int64
	RandomFreq    int64
	RandomCount   int64
}

type NewQuotePayload struct {
	ID        uuid.UUID
	Client    string
	Pair      int64
	Value     int64
	Timestamp time.Time
	Hash      string
}

type ClientConnectPayload struct {
	ID       uuid.UUID
	Client   string
	ClientID int64
}

type ClientDisconnectPayload struct {
	ID       uuid.UUID
	Client   string
	ClientID int64
}

type ReadRandomPayload struct {
	ID       uuid.UUID
	Client   string
	ClientID int64
	Pair     int64
	Count    int64
	Period   int64
}

func (model *ModelQuote) GenerateActions() chan *action.Action {
	ch := make(chan *action.Action, bufferLen)
	go func() {
		model.NewQuoteClientReadConnectDisconnect(ch)
		close(ch)
	}()
	return ch
}

func (model *ModelQuote) NewQuoteClientReadConnectDisconnect(ch chan *action.Action) {

	var number int64

	startDate := time.Now().Unix()
	for start := int64(0); start < model.Duration; start++ {
		for p := int64(0); p < model.Pairs; p++ {
			for f := int64(0); f < model.Frequency; f++ {
				id := uuid.New()
				number++
				ch <- &action.Action{
					ID:     id,
					Client: model.Client,
					Start:  start,
					Number: number,
					Type:   NewQuote,
					Payload: &NewQuotePayload{
						ID:        id,
						Client:    model.Client,
						Pair:      p,
						Timestamp: time.Unix(startDate+start, f),
						Value:     utils.RandomInt64(1000000, 3000000),
					},
				}
			}
		}
	}

	initNumberOfClients := utils.RandomInt(model.Clients)
	numberOfClients := initNumberOfClients

	changeNorm := float64(model.ChangePercent) / float64(10000)

	var changeFreqLoop int64

	for i := int64(0); i < numberOfClients; i++ {
		id := uuid.New()
		number++
		ch <- &action.Action{
			ID:     id,
			Client: model.Client,
			Start:  0,
			Number: number,
			Type:   ClientConnect,
			Payload: &ClientConnectPayload{
				ID:       id,
				Client:   model.Client,
				ClientID: i,
			},
		}

	}

	for start := int64(0); start < model.Duration; start++ {

		change := int64(float64(numberOfClients) * changeNorm)

		if changeFreqLoop >= model.ChangeFreq {
			changeFreqLoop = 0
			if utils.RandomFloat() > 0.5 {
				for i := numberOfClients; i > numberOfClients-change && i > 0; i-- {
					id := uuid.New()
					number++
					ch <- &action.Action{
						ID:     id,
						Client: model.Client,
						Start:  start,
						Number: number,
						Type:   ClientDisconnect,
						Payload: &ClientDisconnectPayload{
							ID:       id,
							Client:   model.Client,
							ClientID: i,
						},
					}
				}

				numberOfClients -= change
				if numberOfClients < 0 {
					numberOfClients = 0
				}

			} else {
				for i := numberOfClients; i < numberOfClients+change; i++ {
					id := uuid.New()
					number++
					ch <- &action.Action{
						ID:     id,
						Client: model.Client,
						Start:  start,
						Number: number,
						Type:   ClientConnect,
						Payload: &ClientConnectPayload{
							ID:       id,
							Client:   model.Client,
							ClientID: i,
						},
					}

				}
				numberOfClients += change
			}
		}
		changeFreqLoop++

		randReadsPerSecond := numberOfClients * model.RandomFreq / secondsInDay

		for i := int64(0); i < randReadsPerSecond; i++ {
			id := uuid.New()
			number++
			ch <- &action.Action{
				ID:     id,
				Client: model.Client,
				Start:  start,
				Number: number,
				Type:   ReadRandom,
				Payload: &ReadRandomPayload{
					ID:       id,
					Client:   model.Client,
					ClientID: utils.RandomInt(numberOfClients),
					Pair:     utils.RandomInt(model.Pairs),
					Count:    utils.RandomInt(model.RandomCount),
					Period:   start,
				},
			}

		}
	}
}
