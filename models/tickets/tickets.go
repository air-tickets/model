package tickets

import (
	"math"

	"github.com/google/uuid"
	"gitlab.com/air-tickets/model/action"
	"gitlab.com/air-tickets/model/utils"
)

const (
	bufferLen     = 100
	secondsInYear = 365 * 24 * 3600
)

type ModelTicket struct {
	Duration     int64
	Purchases    float64
	Client       string
	Passengers   int64
	Airports     int64
	Airlines     int64
	Agencies     int64
	DepartureLag int64
}

func (model *ModelTicket) GenerateActions() chan *action.Action {

	ch := make(chan *action.Action, bufferLen)
	go func() {
		var number int64
		actionsPerSecond := ActionsPerSecond(model.Purchases, model.Passengers)

		for count, start := int64(0), int64(0); start < model.Duration; start++ {
			integer := math.Floor(actionsPerSecond)
			remainder := actionsPerSecond - integer
			if utils.RandomFloat() < remainder {
				integer++
			}
			for i := 0; i < int(integer); i++ {
				number++
				a := &action.Action{
					ID:     uuid.New(),
					Client: model.Client,
					Start:  start,
					Number: number,
					Type:   "Ticket",
				}

				payload := model.CreatePayload()
				payload.ID = a.ID
				payload.Client = a.Client

				a.Payload = payload

				ch <- a

				count++
			}
		}
		close(ch)

	}()

	return ch

}

func ActionsPerSecond(purchases float64, passengers int64) float64 {
	return float64(passengers) * purchases / float64(secondsInYear)
}
