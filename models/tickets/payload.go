package tickets

import (
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/air-tickets/model/utils"
)

type NewTicketPayload struct {
	ID     uuid.UUID
	Client string
	Passenger,
	Departure,
	Destination,
	AirLine,
	AirAgency,
	Delay int64
}

func (p *NewTicketPayload) String() string {
	return fmt.Sprintf("Passenger %d, Departure: %d, Destination: %d, AirLine: %d, AirAgency: %d, Delay: %d, Client: %s",
		p.Passenger,
		p.Departure,
		p.Destination,
		p.AirLine,
		p.AirAgency,
		p.Delay,
		p.Client,
	)
}

func assertPayload(p *NewTicketPayload) bool {
	return p.Departure != p.Destination
}

func (model *ModelTicket) CreatePayload() (p *NewTicketPayload) {
	p = &NewTicketPayload{
		Passenger:   utils.RandomInt(model.Passengers),
		Departure:   utils.RandomInt(model.Airports),
		Destination: utils.RandomInt(model.Airports),
		AirLine:     utils.RandomInt(model.Airlines),
		AirAgency:   utils.RandomInt(model.Agencies),
		Delay:       utils.RandomInt(model.DepartureLag),
	}
	for !assertPayload(p) {
		p.Destination = utils.RandomInt(model.Airports)
	}
	return
}
