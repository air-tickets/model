package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/air-tickets/model/consumers"
	"gitlab.com/air-tickets/model/handler"
	"gitlab.com/air-tickets/model/input"
	"gitlab.com/air-tickets/model/models/quotes"
	"gitlab.com/air-tickets/model/models/tickets"
	"gitlab.com/air-tickets/model/utils"
)

func main() {

	var (
		DataBase       string
		Output         string
		GenerationName string
	)

	var (
		Loop  bool
		Model string
		// Dir   string
	)

	var (
		Airports     int64
		Airlines     int64
		Agencies     int64
		Passengers   int64
		Purchases    float64
		DepartureLag int64
		TestDuration int64

		Client        string
		Pairs         int64
		Frequency     int64
		Clients       int64
		ChangeFreq    int64
		ChangePercent int64
		RandomFreq    int64
		RandomCount   int64
	)

	var rootCmd = &cobra.Command{
		Use: "model",
	}

	var airticketsCmd = &cobra.Command{
		Use:   "airtickets",
		Short: "Generate actions by airtickets model",
		Run: func(cmd *cobra.Command, args []string) {
			ch := (&tickets.ModelTicket{

				Duration:     TestDuration,
				Purchases:    Purchases,
				Client:       Client,
				Passengers:   Passengers,
				Airports:     Airports,
				Airlines:     Airlines,
				Agencies:     Agencies,
				DepartureLag: DepartureLag,
			}).GenerateActions()

			if DataBase != "" {
				dbwriter := consumers.NewDBWriter(DataBase, GenerationName)
				dbwriter.Write(ch)
				dbwriter.Close()
				return
			}

			if Output != "" {
				fwriter := consumers.NewFileWriter(Output)
				fwriter.Write(ch)
				fwriter.Close()
				return
			}

			consumers.WriteConsole(ch)
		},
	}

	var quotesCmd = &cobra.Command{
		Use:   "quotes",
		Short: "Generate actions by quotes model",
		Run: func(cmd *cobra.Command, args []string) {
			ch := (&quotes.ModelQuote{

				Client:        Client,
				Duration:      TestDuration,
				Pairs:         Pairs,
				Frequency:     Frequency,
				Clients:       Clients,
				ChangeFreq:    ChangeFreq,
				ChangePercent: ChangePercent,
				RandomFreq:    RandomFreq,
				RandomCount:   RandomCount,
			}).GenerateActions()

			if DataBase != "" {
				dbwriter := consumers.NewDBWriter(DataBase, GenerationName)
				dbwriter.Write(ch)
				dbwriter.Close()
				return
			}

			if Output != "" {
				fwriter := consumers.NewFileWriter(Output)
				fwriter.Write(ch)
				fwriter.Close()
				return
			}

			consumers.WriteConsole(ch)

		},
	}

	var testCmd = &cobra.Command{
		Use:   "test",
		Short: "Run actions",
		Run: func(cmd *cobra.Command, args []string) {

			reader := input.FileReader{
				Dir: Output,
			}

			switch Model {
			case "airtickets":
				reader.TypeNames = []string{
					"Ticket",
				}

			case "quotes":
				reader.TypeNames = []string{
					"NewQuote",
					"ClientConnect",
					"ClientDisconnect",
					"ReadRandom",
				}
			}

			testHandler := &handler.Handler{}
			testHandler.Exec(reader.Open())
			for Loop {
				testHandler.Exec(reader.Open())
			}
		},
	}

	testCmd.Flags().BoolVarP(&Loop, "loop", "", false, "Whether to loops test")
	testCmd.Flags().StringVarP(&Model, "model", "", "airtickets", "Model for testing")
	// testCmd.Flags().StringVarP(&Dir, "directory", "", "actions", "Directory of actions")

	rootCmd.AddCommand(airticketsCmd)
	rootCmd.AddCommand(quotesCmd)
	rootCmd.AddCommand(testCmd)

	rootCmd.PersistentFlags().StringVarP(&Client, "client", "c", "noname", "Client")
	rootCmd.PersistentFlags().StringVarP(&GenerationName, "generation-name", "n", "noname", "Generation name")
	rootCmd.PersistentFlags().StringVarP(&DataBase, "database", "d", "",
		`Database ["host=localhost port=7432 user=models dbname=models password=testpass sslmode=disable"]`)
	rootCmd.PersistentFlags().StringVarP(&Output, "directory", "o", "actions", "Directory to read from or write to")
	rootCmd.PersistentFlags().Int64VarP(&TestDuration, "duration", "s", 10, "Test duration in sec")

	airticketsCmd.Flags().Int64VarP(&Airports, "airports", "p", 150*150, "Number of airports")
	airticketsCmd.Flags().Int64VarP(&Airlines, "airlines", "l", 30, "Number of airlines")
	airticketsCmd.Flags().Int64VarP(&Agencies, "agencies", "a", 200, "Number of agencies")
	airticketsCmd.Flags().Int64VarP(&Passengers, "passengers", "m", 2e9, "Number of passengers")
	airticketsCmd.Flags().Float64VarP(&Purchases, "purchases", "b", 2, "Average number of purchases per year")
	airticketsCmd.Flags().Int64VarP(&DepartureLag, "lag", "g", 60, "Fly departure lag in days")

	quotesCmd.Flags().Int64VarP(&Pairs, "pairs", "p", 10, "Number of pairs")
	quotesCmd.Flags().Int64VarP(&Frequency, "frequency", "f", 1, "Count of qoutes per second for each pair")
	quotesCmd.Flags().Int64VarP(&Clients, "clients", "C", 100, "Initial average number of clients")
	quotesCmd.Flags().Int64VarP(&ChangeFreq, "change-freq", "F", 1, "Seconds between changing count of clients")
	quotesCmd.Flags().Int64VarP(&ChangePercent, "change-percent", "P", 1000,
		"How many cleints to connect or disconnect in basis points, 1% = 100 basis points")
	quotesCmd.Flags().Int64VarP(&RandomFreq, "random-freq", "r", 10000,
		"How many times client reads quotes in random interval daily")
	quotesCmd.Flags().Int64VarP(&RandomCount, "random-count", "R", 100,
		"Count of quotes to read by client at random interval")

	err := rootCmd.Execute()
	utils.CheckError(err)
}
