package utils

import (
	"crypto/rand"
	"math/big"
	mathRand "math/rand"
	"time"
)

func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}

func RandomInt(n int64) int64 {
	if n == 0 {
		return 0
	}
	max := new(big.Int).SetInt64(n)
	a, _ := rand.Int(rand.Reader, max)
	return a.Int64()
}

func RandomInt64(min, max int64) int64 {
	bmin := new(big.Int).SetInt64(min)
	bmax := new(big.Int).SetInt64(max)
	delta := new(big.Int).Sub(bmax, bmin)
	a, _ := rand.Int(rand.Reader, delta)
	return new(big.Int).Add(a, bmin).Int64()
}

func RandomInt32(min, max int) int {
	seed := mathRand.NewSource(time.Now().UnixNano())
	r := mathRand.New(seed)
	return r.Intn(max-min) + min
}

func RandomFloat() float64 {
	return float64(RandomInt(1e9)) / 1e9
}
