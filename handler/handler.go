package handler

import (
	"encoding/json"
	"log"

	"gitlab.com/air-tickets/model/action"
)

type Handler struct {
	Router map[string]func(a *action.Action)
}

func (hand *Handler) Exec(ch chan *action.Action) {
	for a := range ch {
		if handler, ok := hand.Router[a.Type]; ok {
			handler(a)
		} else {
			defaultHandler(a)
		}
	}
}

func defaultHandler(a *action.Action) {
	b, _ := json.Marshal(a)
	log.Println(string(b))
}
