package input

import (
	"encoding/json"
	"log"
	"os"
	"sync"

	"gitlab.com/air-tickets/model/action"
	"gitlab.com/air-tickets/model/models/quotes"
	"gitlab.com/air-tickets/model/models/tickets"
)

const fileType = ".json"

type FileReader struct {
	Dir       string
	TypeNames []string
}

func (r *FileReader) Open() chan *action.Action {
	ch := make(chan *action.Action)

	go func() {
		var wg = new(sync.WaitGroup)
		for _, typeName := range r.TypeNames {
			file, err := os.Open(r.Dir + "/" + typeName + fileType)
			if err != nil {
				log.Println(err)
			}
			wg.Add(1)
			go readFile(file, typeName, ch, wg)
		}

		wg.Wait()
		close(ch)
	}()
	return ch
}

func blankAction(typeName string) *action.Action {
	var a = new(action.Action)
	switch typeName {
	case "NewQuote":
		a.Payload = &quotes.NewQuotePayload{}

	case "ClientConnect":
		a.Payload = &quotes.ClientConnectPayload{}

	case "ClientDisconnect":
		a.Payload = &quotes.ClientDisconnectPayload{}

	case "ReadRandom":
		a.Payload = &quotes.ReadRandomPayload{}

	case "Ticket":
		a.Payload = &tickets.NewTicketPayload{}
	}

	return a
}

func readFile(file *os.File, typeName string, ch chan *action.Action, wg *sync.WaitGroup) {

	dec := json.NewDecoder(file)

	_, _ = dec.Token()

	for dec.More() {

		a := blankAction(typeName)

		err := dec.Decode(a)

		if err != nil {

			log.Fatal(err)

		}

		ch <- a
	}
	file.Close()
	wg.Done()
}
