package action

import (
	"github.com/google/uuid"
)

type Action struct {
	ID                uuid.UUID   `json:"id"`
	Client            string      `json:"client"`
	GenerationName    string      `json:"-"`
	GenerationVersion int64       `json:"-"`
	Number            int64       `json:"number"`
	Start             int64       `json:"start"`
	Type              string      `json:"type"`
	Payload           interface{} `gorm:"-"`
}
