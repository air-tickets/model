package consumers

import (
	"encoding/json"
	"os"

	"gitlab.com/air-tickets/model/action"
	"gitlab.com/air-tickets/model/utils"
)

type FileWriter struct {
	Folder string
	Files  map[string]*os.File
}

func NewFileWriter(name string) *FileWriter {
	_ = os.Mkdir(name, 0777)
	return &FileWriter{
		Folder: name,
		Files:  make(map[string]*os.File),
	}
}

func (w *FileWriter) Write(ch chan *action.Action) {
	for a := range ch {

		if file, ok := w.Files[a.Type]; !ok {
			file, _ := os.Create(w.Folder + "/" + a.Type + ".json")

			w.Files[a.Type] = file

			_, err := file.Write([]byte("["))
			utils.CheckError(err)

			b, err := json.Marshal(a)
			utils.CheckError(err)

			_, err = file.Write(b)
			utils.CheckError(err)
		} else {
			_, err := file.Write([]byte(","))
			utils.CheckError(err)

			b, err := json.Marshal(a)
			utils.CheckError(err)

			_, err = file.Write(b)
			utils.CheckError(err)
		}
	}
}

func (w *FileWriter) Close() {
	for _, f := range w.Files {
		_, err := f.Write([]byte("]"))
		utils.CheckError(err)
		f.Close()
	}
}
