package consumers

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //postgres driver
	"gitlab.com/air-tickets/model/action"
	"gitlab.com/air-tickets/model/models/quotes"
	"gitlab.com/air-tickets/model/models/tickets"
	"gitlab.com/air-tickets/model/utils"
)

type Generation struct {
	Name    string
	Version int64 `gorm:"AUTO_INCREMENT"`
}

func NewDBWriter(database string, generationName string) *DBWriter {
	db, err := gorm.Open("postgres", database) //"host=localhost port=5432 user=username dbname=testdb password=password")
	utils.CheckError(err)

	db.AutoMigrate(&Generation{})

	db.AutoMigrate(&action.Action{})
	db.AutoMigrate(&quotes.NewQuotePayload{})
	db.AutoMigrate(&quotes.ClientConnectPayload{})
	db.AutoMigrate(&quotes.ClientDisconnectPayload{})
	db.AutoMigrate(&quotes.ReadRandomPayload{})
	db.AutoMigrate(&tickets.NewTicketPayload{})

	generation := &Generation{
		Name: generationName,
	}

	db.Create(generation)

	return &DBWriter{
		DB:         db,
		Generation: generation,
	}
}

type DBWriter struct {
	DB         *gorm.DB
	Generation *Generation
}

func (w *DBWriter) Write(ch chan *action.Action) {
	for a := range ch {
		a.GenerationVersion = w.Generation.Version
		a.GenerationName = w.Generation.Name
		w.DB.Create(a)
		w.DB.Create(a.Payload)
	}
}

func (w *DBWriter) Close() {
	w.DB.Close()
}
