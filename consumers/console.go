package consumers

import (
	"encoding/json"
	"fmt"

	"gitlab.com/air-tickets/model/action"
	"gitlab.com/air-tickets/model/utils"
)

func WriteConsole(ch chan *action.Action) {
	for a := range ch {
		b, err := json.Marshal(a)
		utils.CheckError(err)
		fmt.Println(string(b))
	}
}
